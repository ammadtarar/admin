import axios from "axios";

// const baseURL = "http://localhost:3003/";
// const baseURL = "https://api.nxgen.internal.sawatechnologies.org/";
const baseURL = "https://api.nxgenlabs.com.pk/";

axios.defaults.baseURL = baseURL;

const HTTP = axios.create({
  baseURL: process.env.VUE_APP_API,
  responseType: "json",
});

HTTP.interceptors.request.use(
  function (config) {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = token;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

HTTP.interceptors.response.use(
  (response) => response,
  (error) => {
    console.log(error.response);
    if (error.response.status === 402) {
      console.log("INTERCEPTED 401");
      localStorage.removeItem("email");
      localStorage.removeItem("token");
      localStorage.removeItem("id");
      localStorage.removeItem("name");
      location.reload();
      alert("Session timeout");
    }
    throw error;
  }
);

const URLS = {
  baseURL: baseURL,
  SUPER_ADMIN: {
    LOGIN: "admin/login",
    LIST: "admin/list",
    REGISTER: "admin/register",
    BY_ID: "admin/:id"
  },
  FEEDBACK: {
    LIST: "feedback/list"
  },
  SUBSCRIBERS: {
    LIST: "subscriber/list"
  },
  PATIENTS: {
    LIST: "patient/list"
  },
  APPOINTMENTS: {
    LIST: "appointment/list",
    BY_ID: "appointment/:id"
  },
  FILES: {
    UPLOAD: 'file/upload'
  },
  BANNER: {
    CREATE: 'banner/create',
    BY_ID: 'banner/:id',
    LIST: 'banner/list'
  },
  RIDER: {
    LIST: 'rider/list',
    ASSIGN_RIDE: 'appointment/:id/assign/rider/:riderId'
  },
  PACKAGE: {
    CREATE: 'package/create',
    BY_ID: 'package/:id',
    LIST: 'package/list'
  },
  FAQ: {
    CREATE: 'faq/create',
    BY_ID: 'faq/:id',
    LIST: 'faq/list'
  },
};

export { HTTP, URLS };
