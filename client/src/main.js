import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import Toaster from '@meforma/vue-toaster';

String.prototype.replaceAll = function (str1, str2, ignore) {
    return this.replace(new RegExp(str1.replace(
        /([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ?
            "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") :
        str2);
}

createApp(App).use(router).use(Toaster).mount('#app')
