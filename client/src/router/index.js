import { createRouter, createWebHistory } from 'vue-router'

import Login from '../views/Login.vue';
import Admins from '../views/Admins.vue';
import Patients from '../views/Patients.vue';
import Appointments from '../views/Appointments.vue';
import Feedback from '../views/Feedback.vue';
import Subscribers from '../views/Subscribers.vue';
import Banners from '../views/Banners.vue';
import Packages from '../views/Packages.vue';
import FAQ from '../views/FAQ.vue';

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/admins',
    name: 'admins',
    component: Admins,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/appointments',
    name: 'appointments',
    component: Appointments,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/patients',
    name: 'Patients',
    component: Patients,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/feedback',
    name: 'Feedback',
    component: Feedback,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/subscribers',
    name: 'Subscribers',
    component: Subscribers,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/banners',
    name: 'Banners',
    component: Banners,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/packages',
    name: 'Packages',
    component: Packages,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/faq',
    name: 'FAQ',
    component: FAQ,
    meta: {
      requiresAuth: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token') == null || localStorage.getItem('token') == undefined || localStorage.getItem('token') == '') {
      next({
        path: '/',
        params: { nextUrl: to.fullPath }
      });
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
